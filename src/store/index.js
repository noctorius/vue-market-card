import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    productOrder: [],
    address: null
  },
  getters: {
    getAddress: state => state.address,
    getProductOrders: state => state.productOrder
  },
  mutations: {
    DELETE_ORDER(state, index) {
      console.info('Delete Order', state.productOrder[index])
      state.productOrder.splice(index, 1)
    },
    ADD_ORDER(state, p) {
      state.productOrder.push(p)
    },
    ADD_ADRESS(state, address) {
      state.address = address
    }
  },
  actions: {
    deleteByOrder({commit, state}, name) {
      const index = state.productOrder.findIndex(p => p.product == name)
      commit('DELETE_ORDER', index)
    },
    addOrder({ commit, state }, p) {
      commit('ADD_ORDER', p)
    },
    addAdress({ commit, state }, address) {
      commit('ADD_ADRESS', address)
    }
  },
  modules: {
  }
})
